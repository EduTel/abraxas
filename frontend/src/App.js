import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';

import { Formulario } from "./components/formulario";
import { Lista } from "./components/lista";
const axios = require('axios');

function App() {
  let default_formulario = {
    "id": "",
    "url": "",
    "descripcion": "",
    "duracion": "",
    "estatus": 0,
    "tiempo_registrado": "",
    "created_at": "",
    "updated_at": ""
  }

  const [formulario, setFormulario] = useState(default_formulario);
  const [datos, setDatos] = useState([]);

  // De forma similar a componentDidMount y componentDidUpdate
  useEffect(() => {
    axios.get(`http://localhost:8000/tasks/api/task/`)
    .then(res => {
        const data = res.data;
        setDatos(data)
        console.log(data)
    })
  },[]);

  return (
    <div className="App">
      <div className="container mt-5">
        <div className="col-12">
          <h2 className="text-center">TASKS</h2>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Formulario
              formulario = {formulario}
              setFormulario = {setFormulario}
              default_formulario = {default_formulario}
              datos = {datos}
              setDatos = {setDatos}
            />
          </div>
          <div className="col-md-6">
            <Lista
              datos = {datos}
              setDatos = {setDatos}
              setFormulario = {setFormulario}
            />
          </div>
      </div>
      </div>
    </div>
  );
}

export default App;
