import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const Alerta = ({mensaje}) => {
    return(
        <div className="alert alert-danger" role="alert">
            {mensaje}
        </div>
    )
}

Alerta.propTypes = {
    mensaje: PropTypes.string
};

export {Alerta}