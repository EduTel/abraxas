import React from 'react';
import PropTypes from 'prop-types';
const axios = require('axios');

const Lista = ({datos,setDatos, setFormulario}) => {

    const helperDelete = (event,id) => {
        console.log(event)
        axios.delete(`http://localhost:8000/tasks/api/task/${id}/`)
        .then(res => {
            setDatos(
                datos.filter(formulario => formulario.id !== id)
            )
            alert("Dato Eliminado")
        })
    }
    const helperEdit = (event,id) => {
        console.log("==================helperEdit")
        console.log(event)
        console.log(datos)
        let dato_editar = datos.filter(formulario => formulario.id === id)
        console.log(dato_editar[0])
        setFormulario(dato_editar[0])
    }
    var datos_reverse = [...datos].reverse()
    const lista = datos_reverse.map((formulario)=>
        <div className="col-md-12" key={formulario.id} id={formulario.id}>
            <div className="card p-2 m-2" styles={{width: '18rem'}}>
                <div className="card-body">
                    <p className="card-text"> <span className="negritas">id: </span> {formulario.id}</p>
                    <p className="card-text"> <span className="negritas">descripcion: </span> {formulario.descripcion}</p>
                    <p className="card-text"> <span className="negritas">duracion: </span> {formulario.duracion}</p>
                    <p className="card-text"> <span className="negritas">estatus: </span> { (formulario.estatus==0)?"Pendiente":"Completada" }</p>
                    <p className="card-text"> <span className="negritas">tiempo_registrado: </span> { (formulario.tiempo_registrado=="")?"Sin registrar":formulario.tiempo_registrado }</p>
                    <p className="card-text"> <span className="negritas">created_at: </span> {formulario.created_at}</p>
                    <p className="card-text"> <span className="negritas">updated_at: </span> {formulario.updated_at}</p>
                    <div className="row">
                        <div className="col-md-6">
                            <button type="button" onClick={(event) => helperDelete(event, formulario.id)} className="btn btn-danger btn-block btn-sm">Eliminar</button>
                        </div>
                        <div className="col-md-6">
                            {formulario.estatus==0?<button type="button" onClick={(event) => helperEdit(event, formulario.id)} className="btn btn-warning btn-block btn-sm">Editar</button>:""}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
    return (
        <React.Fragment>
            <div className="row">
                {lista.length>0?lista:""}
            </div>
        </React.Fragment>
    );
}
Lista.propTypes = {
    formularios: PropTypes.array,
    setFormularios: PropTypes.func
};

export {Lista}