import React, { useState, useEffect } from 'react';
import add from './../static/add-file.png';
import { Alerta } from "./alert";
import { v4 as uuidv4 } from 'uuid';
import PropTypes from 'prop-types';
const axios = require('axios');

const Formulario = ({formulario, setFormulario, default_formulario, datos, setDatos}) => {
    const [validacionformulario, setValidacionFormulario] = useState("")
    const {id,descripcion,duracion,estatus} = formulario
    const onChange_setFormulario = (event) => {
        console.log(event)
        setFormulario({
            ...formulario,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        if(descripcion.trim()==="" || String(duracion).trim()==="" || estatus===""){
            setValidacionFormulario("Todos los campos son requeridos")
            return;
        }
        setValidacionFormulario("")
        console.log("formulario: ")
        console.log(formulario)
        if(id===""){
            axios.post(`http://localhost:8000/tasks/api/task/`,formulario)
            .then(res => {
                let data = res.data;
                setDatos([
                    ...datos,
                    data
                ])
                setFormulario(default_formulario)
                alert("Datos guardados")
            })
        }else{
            axios.put(`http://localhost:8000/tasks/api/task/${id}/`,formulario)
            .then(res => {
                let data = res.data;
                console.log("data", datos)
                datos = datos.filter(formulario => formulario.id !== id)
                setDatos([
                    ...datos,
                    data
                ])
                setFormulario(default_formulario)
                alert("Datos guardados")
            })
        }
    }
    return (
        <React.Fragment>
            { validacionformulario!=="" ? <Alerta mensaje={validacionformulario}/> : null}
            <form onSubmit={handleSubmit}>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label htmlFor="input_id">ID</label>
                        <input disabled type="text" className="form-control" name="id" id="input_id" value={id} onChange={onChange_setFormulario} />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label htmlFor="input_descripcion">Descripcion</label>
                        <input type="text" className="form-control" name="descripcion" id="input_descripcion" value={descripcion} onChange={onChange_setFormulario} />
                    </div>
                    <div className="form-group col-md-6">
                        <label htmlFor="input_duracion">Duracion</label>
                        <input type="number" className="form-control" name="duracion" id="input_duracion" value={duracion} onChange={onChange_setFormulario} />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label htmlFor="input_estatus">Estatus</label>
                        <select className="form-control" name="estatus" id="input_estatus" aria-label="Default select example" onChange={onChange_setFormulario} >
                            <option vselected alue="0">INCOMPLETO</option>
                            <option value="1">COMPLETADO</option>
                        </select>
                    </div>
                </div>
                {/*<input type="image" src={add} alt="Submit" width="48" height="48"></input>*/}
                <div className="row">
                    <div className="col-md-6">
                        <button type="submit" className="btn btn-primary btn-block btn-sm">Agregar/Editar</button>
                    </div>
                    <div className="col-md-6">
                        <button type="button" onClick={(event) => setFormulario(default_formulario)} className="btn btn-secondary btn-block btn-sm">Limpiar</button>
                    </div>
                </div>
            </form>
        </React.Fragment>
    );
}

Formulario.propTypes = {
    formulario: PropTypes.object,
    setFormulario: PropTypes.func,
    default_formulario: PropTypes.object,
    datos: PropTypes.array,
    setDatos: PropTypes.func
};

export {Formulario}