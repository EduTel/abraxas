# Primer implementacion
Nota: El campo "tiempo_registrado" deberia ser un campo Calculado y no deberia estar en la bd ya que se obtiene apartir de otros campos
## Admin Django:
user: edutel  
password: 12345  

## Ejecutar en local:
* pre-requisitos:
    1. debes tener instalado python
    2. debes tener instalado node

* activacion:  
    * backend (Django y Django Rest Framework) 
        1. source bin/activate
        2. python manage.py runserver 
        3. ir a la url http://127.0.0.1:8000/tasks/
    * frontend
        1. cd frontend
        2. npm install
        3. npm start
        4. ir a la url http://localhost:3000/
    * backend y frontend el mismo servidor
        1. cd frontend
        2. npm install
        3. npm run build
        4. seguir los pasos de "backend"

# Docker:
1. cd frontend
2. npm install
3. npm run build
4. docker build -t abraxas_local:v0.0.1 . 
5. docker run -it --publish 8000:8000 abraxas_local:v0.0.1
6. ir a http://localhost:8000/tasks/

## Docker hub:
url: https://hub.docker.com/repository/docker/edutel/abraxas

1. docker run -it --publish 8000:8000 edutel/abraxas:v0.0.1

## Gitlab:
https://gitlab.com/EduTel/abraxas

# Despliegue:
* Heroku
    * https://edutel-abraxas.herokuapp.com/tasks/
    * https://edutel-abraxas.herokuapp.com/admin/login/?next=/admin/
    * https://edutel-abraxas.herokuapp.com/tasks/api/task/
* aws
    * https://container--abraxas.p30of7mlhm09g.us-east-1.cs.amazonlightsail.com/tasks/
    * https://container--abraxas.p30of7mlhm09g.us-east-1.cs.amazonlightsail.com/admin/login/?next=/admin/
    * https://container--abraxas.p30of7mlhm09g.us-east-1.cs.amazonlightsail.com/tasks/api/task/

## SCRAPING Selenium "pre-llenado" (esta probado con chrome en mac m1):
* path: /task/scraping_task.py  

para ejecutar se necesita el driver https://chromedriver.storage.googleapis.com/index.html?path=91.0.4472.19/ de ser necesario pasar el parametro **executable_path** ejemplo:  
driver = webdriver.Chrome(executable_path=r"~/chromedriver")

## ejecutar pruebas unitarias (se puede automatizar con git hook o con jenkins):
1. python manage.py test (se ejecutara el archivo task/tests.py) la pruebas unitarias estan echas segun el estandar de Django Rest Framework (ahi es donde y como se deberian llenar los datos del api segun Django)

## Documentacion:
backend Django esta echo con el estandar pydoct de python y tambien esta implementado el "Admin documentation generator " de Django http://127.0.0.1:8000/admin/doc antes debe de estar logueado con el usuario y passsword proporcionados anteriormente.  
urls documentadas:  
* http://127.0.0.1:8000/admin/doc/models/task.task/
* http://127.0.0.1:8000/admin/doc/views/task.views.TaskViewSet/

paths:  
* task/models.py
* task/scraping_task.py
* task/serializers.py
* task/views.py
* task/tests.py

# Segunda implementacion
path: node_backend/  
tecnologias: mongo, mongo atlas, mongoose, Graphql, Node  

# Docker:
1. cd node_backend
2. docker-compose up -d
3. ir a http://localhost:4001/

## Ejecutar en local
* pre-requisitos:
    1. debes tener instalado mongodb
    2. debes tener instalado node

* activacion:  
    * backend (Node, Graphql) 
        1. cd node_backend
        2. npm install
        3. node index.js  
        4. ir a la url http://localhost:4000/
* probar la api Graphql
```
query Tasks {
  Tasks{
    ..._Tasks
  }
}
fragment _Tasks on Tasks {
    _id
    Descripcion
    Duracion
    Tiempo_registrado
    Estatus
    created_at
    updated_at
}
mutation addTasks {
  addTasks(input:{
                        Descripcion: "desc 03",
                        Duracion: 40,
                        Estatus: Pendiente
                    }
                ){
    ..._Tasks
  }
}
mutation updateTasks {
  updateTasks(id: "5fd6c00a23445f11063fb739",input:{
                                                        Descripcion: "desc 01 :)",
                                                        Duracion: 11,
                                                        Estatus: Pendiente
                                                    }){
    code
    success
    message
    tasks {
      ..._Tasks
    }
  }
}
mutation deleteTasks{
  deleteTasks(id: "60bfea89cb57fb2f3c6eefd9")
}
```

