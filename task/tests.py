from django.test import TestCase

# Create your tests here.
from django.urls import include, path, reverse
from rest_framework.test import APITestCase, URLPatternsTestCase
from rest_framework import status
from .models import Task
from rest_framework.test import APIClient
from pprint import pprint

class TaskApiTests(APITestCase):
    """
        Esta clase es para las pruebas unitarias
    """
    client = APIClient()

    def setUp(self):
        """
            valores de entrada para hacer pruebas unitarias
        """
        Task.objects.create(descripcion='test desc1 :)', duracion=10, estatus=0)
        Task.objects.create(descripcion='test desc1 XD', duracion=10, estatus=1) # No deberia poder editar

    def test_get(self):
        """
            Test sobre el metodo GET
        """
        #url = reverse()
        response = self.client.get('/tasks/api/task/1/', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        output = {
            "descripcion": response.data["descripcion"],
            "duracion": response.data["duracion"],
            "estatus": response.data["estatus"]
        }
        self.assertEqual(output, {'descripcion': 'test desc1 :)', "duracion": 10, "estatus" : 0})

    def test_update(self):
        """
            Test sobre el metodo PUT
        """
        data = {
                "descripcion": 'test desc1 :(',
                "duracion": 11,
                "estatus": 1
            }
        response = self.client.put('/tasks/api/task/1/', data, format='json')
        print("response.data",response.data)
        output_filter = {
            "descripcion": response.data["descripcion"],
            "duracion": response.data["duracion"],
            "estatus": response.data["estatus"]
        }
        print("response.status_code", response.status_code)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], 1)
        self.assertEqual(output_filter, data)

    def test_update_status_1(self):
        """
            Test sobre el metodo PUT intentando modificar sobre el status "completada" que no deberia ser posible
        """
        data = {
                "descripcion": 'test desc1 XD',
                "duracion": 10,
                "estatus": 0
            }
        response = self.client.put('/tasks/api/task/2/', data, format='json')
        print("response.status_code", response.status_code)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete(self):
        """
            Test sobre el metodo DELETE primero deberia eliminarse y despues el resultado del metodo GET deberia ser HTTP_404_NOT_FOUND
        """
        data = {
                "descripcion": 'test desc1 XD',
                "duracion": 10,
                "estatus": 0
            }
        response = self.client.delete('/tasks/api/task/2/', data, format='json')
        print("response.status_code", response.status_code)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.get('/tasks/api/task/2/', format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)