from django.contrib import admin
from .models import Task

# Register your models here.
class TodoAdmin(admin.ModelAdmin):
    list_display = ('descripcion', 'duracion', 'estatus')

# Register your models here.

admin.site.register(Task, TodoAdmin)