# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.proxy import *
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
from selenium.common import exceptions
import time
import os

print(os.path.dirname(__file__))

class Task:
    """
        para usar selenium usaremos lo que se llama Clase por pantalla,
        asi que usaremos una clase llamada TASK que seria la unica pantalla
        que tenemos
    """
    url = "http://127.0.0.1:8000/tasks/"
    driver = webdriver.Chrome()

    def __init__(self):
        """
            seleccionamos en ChromeDriver, Abrimos el navegador y establecemos
            el tiempo global que esperara el navegador entre cada tarea
        """
        chrome_options = webdriver.ChromeOptions()
        print("***********************************Current session is {}".format(self.driver.session_id))
        try:
            self.driver.get(self.url)
            self.driver.implicitly_wait(25)
            self.driver.maximize_window()
        except exceptions.InvalidSessionIdException as e:
            print(e)
    
    def fillForm(self,dataForm={}):
        """
            Empezamos a seleccionamos con XPATH (que es la manera mas estandarizada)
            los campos que vamos a llenar
        """
        print("===================================fillForm")
        try:
            imput_descripcion = self.driver.find_element_by_xpath('//*[@id="input_descripcion"]')
            imput_descripcion.send_keys(dataForm["descripcion"])
            imput_duracion = self.driver.find_element_by_xpath('//*[@id="input_duracion"]')
            imput_duracion.send_keys(dataForm["duracion"])
            imput_submit = self.driver.find_element_by_xpath('//*[@id="root"]/div/div/div[2]/div[1]/form/input')
            imput_submit.click()
            frame_alert = self.driver.switch_to.alert()
            frame_alert.accept()
        except NoSuchElementException:
            return False
        return True

    def quit(self):
        self.driver.quit()
        return True


if __name__ == "__main__":
    start = Task()
    if start.fillForm({"descripcion": "desc scraping","duracion": 10 }):
        start.quit()