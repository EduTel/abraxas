from django.shortcuts import render
from .models import Task
from .serializers import TaskSerializer
from rest_framework import routers, serializers, viewsets
from rest_framework.response import Response
from rest_framework import viewsets, status
from pprint import pprint
from django.core import serializers
import json


# Create your views here.
# ViewSets define the view behavior.
class TaskViewSet(viewsets.ModelViewSet):
    """
        aqui se establecen todos los campos a ser mostrados en la api con Django Rest Framework
    """
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    #def update(self, request, *args, **kwargs):
    #    response = {'message': 'Este campo no se puede actualizar'}
    #    partial = kwargs.pop('partial', False)
    #    instance = self.get_object()
    #    serializer = self.get_serializer(instance, data=request.data, partial=partial)
    #    serializer.is_valid(raise_exception=True)
    #    self.perform_update(serializer)
    #    pprint(kwargs)
    #    data = Task.objects.get(pk=kwargs["pk"])
    #    if data.estatus==1:
    #        print("************403")
    #        print("request",request.data)
    #        print("args",args)
    #        print("kwargs",kwargs)
    #        print("serializer", serializer)
    #        print("instance", instance)
    #        print(instance.estatus)
    #        serialized_obj = serializers.serialize('json', [ data, ])
    #        pprint(json.dumps(serialized_obj))
    #        return Response(response, status=status.HTTP_403_FORBIDDEN)
    #    else:
    #        super(TaskViewSet, self).update(request, *args, **kwargs)
    #        #response = Task.objects.get(pk=kwargs["pk"])
    #        #serialized_obj = serializers.serialize('json', [ response, ])
    #        #print(serialized_obj)
    #        #return Response(json.loads(serialized_obj), status=status.HTTP_200_OK)
    #        return Response(serializer.data, status=status.HTTP_200_OK)

def index(request):
    return render(request, "build/index.html")