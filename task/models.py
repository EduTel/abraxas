from django.db import models
from datetime import date, datetime, time, timedelta
from django.http import HttpResponse
from rest_framework import serializers

class estatusChoice(models.IntegerChoices):
    """
        Se crea uns subclase de "IntegerChoices" para solo poder seleccionar
        uno de los dos estatus aqui definidos
    """
    INCOMPLETO = 0, 'INCOMPLETO'
    COMPLETADO = 1, 'COMPLETADO'

class Task(models.Model):
    """
        representacion de products_product en la bd
    """

    descripcion = models.CharField(verbose_name="Descripción", max_length=150)
    duracion = models.IntegerField(verbose_name="Duración")
    # tiempo_registrado = models.CharField(verbose_name="Price", max_length=150)
    estatus = models.IntegerField(default=estatusChoice.INCOMPLETO, choices=estatusChoice.choices)
    created_at = models.DateTimeField(verbose_name="Fecha de creacion", auto_now_add=True, null=True)
    updated_at = models.DateTimeField(verbose_name="Fecha de actualizacion", auto_now=True, null=True)

    @property
    def tiempo_registrado(self):
        """
            El campo "tiempo_registrado" deberia ser un campo Calculado 
            y no deberia estar en la bd ya que se obtiene apartir de otros campos
        """
        if self.estatus==1:
            diffTime = self.updated_at-self.created_at
            #print("******diffTime: ",type(diffTime), diffTime)
            if diffTime<=timedelta(minutes=30):
                return "Corto"
            elif diffTime<=timedelta(minutes=45):
                return "Mediano"
            elif diffTime<=timedelta(minutes=60):
                return "Largo"
        return False

    def save(self, *args, **kwargs):
        """
            Para impedir que incluso desde la interfaz de "python manager.py dbshell"
            se pueda editar los campos con status "Completada”
        """
        if self.id: # cuando es un update
            print("update: ")
            data_update = Task.objects.get(pk=self.id)
            print("estatus: ", data_update.estatus)
            if data_update.estatus==0:
                print("Actualizar")
                super(Task, self).save(*args, **kwargs)
            else:
                raise serializers.ValidationError("Este campo no se puede actualizar")
        else:
            super(Task, self).save(*args, **kwargs)

    def __str__(self):
        return "%s" % (self.id)
