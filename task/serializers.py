from .models import Task
from rest_framework import routers, serializers, viewsets


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.DateTimeField(read_only=True, input_formats='%m-%Y', format='%d-%m-%Y %H:%M:%S')
    updated_at = serializers.DateTimeField(read_only=True, input_formats='%m-%Y', format='%d-%m-%Y %H:%M:%S')

    class Meta:
        """
        """
        model = Task
        fields = ["id","url","descripcion","duracion","estatus","tiempo_registrado","created_at","updated_at"]
