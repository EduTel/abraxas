FROM python:3.9.1-alpine3.13
LABEL version="1.0"
LABEL description="Abraxas Django"
LABEL maintainer = ["eduardo_jonathan@outlook.com"]
WORKDIR /app
EXPOSE 8000/tcp

COPY requirements.txt /app
COPY db.sqlite3 /app
COPY abraxas/ /app/abraxas/
COPY manage.py /app
COPY frontend/build/ /app/frontend/build/
COPY task/ /app/task/
#RUN pip install -r /app/requirements.txt
RUN pip3  --no-cache-dir --use-feature=2020-resolver install -r /app/requirements.txt  --no-deps
CMD ["python3", "./manage.py", "runserver", "0.0.0.0:8000"]