const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UsersSchema = new Schema({
    Descripcion: { type: String, required: true, trim: true },
    Duracion: { type: Number},
    Estatus: { type: String, required: true, trim: true},
    created_at: { type: Date, required: true, default: Date.now },
    updated_at: { type: Date, required: true, default: Date.now }
});
UsersSchema.virtual('Tiempo_registrado').get(function () {
    if(this.Estatus==1){
        diffTime = this.updated_at-this.created_at
        console.log("diffTime", diffTime)
        var MS_PER_MINUTE = 60000;
        if(diffTime<=new Date(30 * MS_PER_MINUTE)){
            return "Corto"
        }else if(diffTime<=Date(45 * MS_PER_MINUTE)){
            return  "Mediano"
        }else if(diffTime<=Date(60 * MS_PER_MINUTE)){
            return  "Largo"
        }
    }
    return "Sin registrar"
});
UsersSchema.pre('save', function(next){
    now = new Date();
    this.updated_at = now;
    if ( !this.created_at ) {
      this.created_at = now;
    }
    next();
});
const modelTasks = mongoose.model('Tasks', UsersSchema);
module.exports = {
    modelTasks
}