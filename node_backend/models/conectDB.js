const mongoose = require('mongoose');
require('dotenv').config()
console.log(process.cwd())

conectDB = async ()=>{
    try {
        const conect = process.env.TESTDDBB
        console.log("===============================")
        console.log(conect)
        console.log("===============================")
        const mongooseconnection = await mongoose.connect(conect, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        });
        const connection = mongooseconnection.connection;
        connection.db.listCollections().toArray(function(err, names) {
            if (err) {
                console.log(err);
            } else {
                for (i = 0; i < names.length; i++) {
                    console.log("Table: ",names[i].name);
                }
            }
        });
        console.log("DB conectada")
        return true
    } catch (error) {
        console.error(error)
        process.exit()
    }

}

module.exports = conectDB
