var express = require('express');

const { ApolloServer } = require('apollo-server');
const resolvers = require('./gql/resolvers');
const typeDefs = require('./gql/typeDefs');

const {modelTasks} = require('./models/index');
//const connection = require('./connection');

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({ typeDefs, resolvers });

var conectDB = require('./models/conectDB');

conectDB().then(()=>{
  // The `listen` method launches a web server.
  server.listen({ port: process.env.PORT || 4000 }).then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
  });
})
