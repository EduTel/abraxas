const { gql } = require('apollo-server');

// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.
  interface MutationResponse {
    code: String!
    success: Boolean!
    message: String!
  }
  enum statusTasks {
    Pendiente
    Completada
  }
  type UpdateTasksResponse implements MutationResponse {
    code: String!
    success: Boolean!
    message: String!
    tasks: Tasks
  }
  type Tasks {
    _id: ID
    Descripcion: String
    Duracion: String
    Tiempo_registrado: String
    Estatus: statusTasks
    created_at: String
    updated_at: String
  }
  input PostTasks{
    Descripcion: String!
    Duracion: Int!
    Estatus: statusTasks!
  }
  # *************************Notes

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  # case, the "books" query returns an array of zero or more Books (defined above).
  type Query{
    Tasks(input: [ID]): [Tasks]
  }
  type Mutation {
    addTasks(input: PostTasks): Tasks
    updateTasks(id: ID!, input: PostTasks): UpdateTasksResponse
    deleteTasks(id: ID!): Boolean
  }
`;

module.exports = typeDefs
