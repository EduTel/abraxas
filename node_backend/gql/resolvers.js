// Resolvers define the technique for fetching the types defined in the
// schema. This resolver retrieves books from the "books" array above.
const {modelTasks} = require('./../models/index');
const resolvers = {
    statusTasks: {
        Pendiente: 'Pendiente',
        Completada: 'Completada'
    },
    Query: {
        Tasks: async (parent, args, context, info) => {
            //console.log("info", info)
            //console.log("context", context)
            return await modelTasks.find({}, function(err, Tasks) {
                console.log("Tasks",Tasks)
            })
        }
    },
    Mutation: {
        addTasks: async (parent, args, context, info) => {
            console.log("input", args.input)
            const { Descripcion, Duracion, Estatus } = args.input
            //console.log(context.decodedToken)
            const doc = new modelTasks({Descripcion, Duracion, Estatus});
            const result = await doc.save();
            console.log(result)
            return result

        },
        updateTasks: async (parent, args, context, info) => {
            const { Descripcion, Duracion, Estatus } = args.input
            const existsTasks = await modelTasks.findOne({$and:[{
                    _id: args.id,
                }]
            });
            console.log("existsTasks",existsTasks)
            if(existsTasks==null){
                throw new Error('ID no encontrado');
            }else if (existsTasks?.Estatus=="Completada") {
                throw new Error('La Tasks NO puede ser actualizada por que su estatus es “Completada”.');
            }
            const doc = await modelTasks.findByIdAndUpdate({_id: args.id},{ $set: {Descripcion, Duracion, Estatus} }, {new: true});
            console.log("doc",doc)
            return {
                code: "201",
                success: true,
                message: "was successfully updated",
                tasks: doc
            } // return null if do not update
        },
        deleteTasks: async (parent, args, context, info) => {
            const doc = await modelTasks.findByIdAndDelete({_id: args.id});
            //console.log(doc)
            return (doc===null?false:true) // return null if do not update
        },
    }
};
module.exports = resolvers
