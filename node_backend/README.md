path: node_backend/  
tecnologias: mongo, mongo atlas, mongoose, Graphql, Node  

# Docker:
1. cd node_backend
2. docker-compose up -d
3. ir a http://localhost:4001/

## Ejecutar en local
* pre-requisitos:
    1. debes tener instalado mongodb
    2. debes tener instalado node

* activacion:  
    * backend (Node, Graphql) 
        1. cd node_backend
        2. npm install
        3. node index.js  
        4. ir a la url http://localhost:4000/
* probar la api Graphql
```
query Tasks {
  Tasks{
    ..._Tasks
  }
}
fragment _Tasks on Tasks {
    _id
    Descripcion
    Duracion
    Tiempo_registrado
    Estatus
    created_at
    updated_at
}
mutation addTasks {
  addTasks(input:{
                        Descripcion: "desc 03",
                        Duracion: 40,
                        Estatus: Pendiente
                    }
                ){
    ..._Tasks
  }
}
mutation updateTasks {
  updateTasks(id: "5fd6c00a23445f11063fb739",input:{
                                                        Descripcion: "desc 01 :)",
                                                        Duracion: 11,
                                                        Estatus: Pendiente
                                                    }){
    code
    success
    message
    tasks {
      ..._Tasks
    }
  }
}
mutation deleteTasks{
  deleteTasks(id: "60bfea89cb57fb2f3c6eefd9")
}
```

